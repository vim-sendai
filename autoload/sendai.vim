" Send text to an external file.
" Author: Evan Hanson <evhan@foldling.org>
" Last Change: 2019-06-03
" Version: 0.0.3
" Homepage: http://git.foldling.org/vim-sendai.git
" Repository: http://git.foldling.org/vim-sendai.git
" License: BSD

fun! sendai#Initialize(options)
  for [key, value] in items(a:options)
    if !exists("g:sendai_" . key)
      execute "let g:sendai_" . key . " = " . string(value)
    endif
  endfor

  execute "nnoremap <silent>" . g:sendai_normal_leader . " :set opfunc=sendai#Motion<cr>g@"
  execute "vnoremap <silent>" . g:sendai_visual_leader . " :<c-u>call sendai#Motion('visual')<cr>"
endfun

fun! sendai#Motion(type)
  if exists("b:sendai_command")
    let sendai_command = b:sendai_command
  elseif exists("g:sendai_command")
    let sendai_command = g:sendai_command
  else
    let sendai_command = g:sendai_command_default
    let command_warn = 1
  endif

  if exists("b:sendai_file")
    let sendai_file = b:sendai_file
  elseif exists("g:sendai_file")
    let sendai_file = g:sendai_file
  else
    let sendai_file = g:sendai_file_default
    let file_warn = exists('l:command_warn')
  endif

  let stashed = @@

  if a:type == "visual"
    execute "silent normal! `<" . visualmode() . "`>y"
  elseif a:type == "line"
    execute "silent normal! '[V']y"
  elseif a:type == "char"
    execute "silent normal! `[v`]y"
  elseif a:type == "block"
    execute "silent normal! `[\<C-V>`]y"
  endif

  call system(sendai_command . " > " . fnameescape(sendai_file), @@ . "\n")

  let @@ = stashed

  if exists("l:file_warn")
    echohl WarningMsg
      echo "Warning: No target defined. Using \"" . g:sendai_file_default . "\""
    echohl None
  end
endfun

fun! sendai#SetBuffer(file)
  call sendai#SetBufferFile(file)
endfun

fun! sendai#SetGlobal(file)
  call sendai#SetGlobalFile(file)
endfun

fun! sendai#SetBufferFile(file)
  let b:sendai_file = a:file
endfun

fun! sendai#SetGlobalFile(file)
  let g:sendai_file = a:file
endfun

fun! sendai#SetBufferCommand(command)
  let b:sendai_command = a:command
endfun

fun! sendai#SetGlobalCommand(command)
  let g:sendai_command = a:command
endfun
