vim-sendai
==========
Operators for sending text to an external file.

Usage
-----
This plugin adds an operator that can be combined with other Vim motions
to write text to an external file or command, typically a REPL. By
default, this operator is mapped to the key sequence `gs`. The output
destination is selected by calling `sendai#SetGlobalFile(filename)` or
`sendai#SetGlobalCommand(pipeline)`.

For more information, refer to the plugin's [help file][help].

[help]: https://git.foldling.org/vim-sendai.git/tree/master/doc/sendai.txt

Licence
------
Copyright © 2014-2019 Evan Hanson <evhan@foldling.org>

BSD-style license. See `LICENSE` for details.
