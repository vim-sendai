" Send text to an external file.
" Author: Evan Hanson <evhan@foldling.org>
" Last Change: 2019-06-03
" Version: 0.0.3
" Homepage: http://git.foldling.org/vim-sendai.git
" Repository: http://git.foldling.org/vim-sendai.git
" License: BSD

if exists('g:sendai_loaded') || &compatible || version < 700
  finish
endif

call sendai#Initialize({
\   'file_default'    : '/dev/null'
\ , 'command_default' : 'cat'
\ , 'visual_leader'   : 'gs'
\ , 'normal_leader'   : 'gs'
\ })

let g:sendai_loaded = 1
